# Linux

## System requirements

1. Bash
2. Curl

# Windows

PowerShell 5.0 or later is required to run the script. PowerShell scripts can run 
in bath DOS command-line or PowerShell.

To run a script from DOS command-line

```c:\>powershell -executionpolicy bypass <path to script>```

To run a script from PowerShell prompt, you need to allow execution once. The below
command temporary allow scripts to be executed in the current PowerShell session.

```Set-ExecutionPolicy Bypass -scope Process```

After that, a script can be run by

```PS c:\>.\<script name>```

# How to patch REST bundle(s) in TeamWork Cloud installation

1. Download a patch package and extract to a temporary directory.
2. The file format is _\<bundle\_name\>\_version.jar_. Note the list of file names.
3. Copy new files into _\<twcdir\>/plugins_.
4. Remove Jar files with the same bundle name from _\<twcdir\>/plugins_
5. Open _\<twcdir\>/configuration/config.ini_ with a text editor
6. For each bundle names, look for it in _config.ini_, edit a filename to match the new one.
7. Restart TeamWork Cloud