﻿$username = "Administrator"
$password = "Administrator"
$restbaseurl = "http://127.0.0.1:8111"

$securepassword = ConvertTo-SecureString $password -AsPlainText -Force
$credentials = New-Object System.Management.Automation.PSCredential($username, $securepassword)

[System.Net.ServicePointManager]::ServerCertificateValidationCallback = {$true};


$text = Get-Content   users.txt
for ($i=0;$i -lt $text.length;++$i) {
    $username = $text[$i].Split(":")[0];
    $password = $text[$i].Split(":")[1];

    $input = @{
        "userName" = $username
        "password"=  $password
        "enabled" = "true"
    }

    $j = ConvertTo-Json $input
    
    Invoke-WebRequest -ContentType "application/json" -Uri "$restbaseurl/osmc/admin/users" -Method Post -Credential $credentials  -body $j
  #  Invoke-WebRequest -Uri "$restbaseurl/osmc/admin/users" -Method Get -Credential $credentials 
}