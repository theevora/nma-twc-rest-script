/*
 * AgingCookieStore
 *
 * Revision $Revision$ $Date$
 * Author: Theewara V. (theewara_v@nomagicasia.com)
 *
 * Copyright (c) 2009 - 2017 NoMagic, Inc. All Rights Reserved.
 */
package com.nomagic.esi.rest.modelloader;

import java.util.Date;
import java.util.List;

import org.apache.http.cookie.Cookie;
import org.apache.http.impl.client.BasicCookieStore;

/**
 * @author Theewara V. (theewara_v@nomagicasia.com)
 */
public class AgingCookieStore extends BasicCookieStore {

	private static final long serialVersionUID = 1119245140559544047L;

	private int age;

	private final int ttl;

	public AgingCookieStore(int ttl)
	{
		super();
		age = 0;
		this.ttl = ttl;
	}

	@Override
	public synchronized void clear()
	{
		age = 0;
		super.clear();
	}


	@Override
	public synchronized boolean clearExpired(Date date)
	{
		age = 0;
		return super.clearExpired(date);
	}

	/**
	 * @param string
	 * @return
	 */
	public Object get(String string)
	{
		for (Cookie cookie : getCookies()) {
			if (cookie.getName().equalsIgnoreCase(string)) {
				return cookie.getValue();
			}
		}

		return null;
	}

	/**
	 * Return a age.
	 * @return the age
	 */
	public int getAge()
	{
		return age;
	}

	@Override
	public synchronized List<Cookie> getCookies()
	{
		++age;
		return super.getCookies();
	}

	public boolean isAlive()
	{
		return age < ttl;
	}


}
