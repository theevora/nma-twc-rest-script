/*
 * URIPathBuilder
 *
 * Revision $Revision$ $Date$
 * Author: Theewara V. (theewara_v@nomagicasia.com)
 *
 * Copyright (c) 2009 - 2017 NoMagic, Inc. All Rights Reserved.
 */
package com.nomagic.esi.rest.modelloader;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Theewara V. (theewara_v@nomagicasia.com)
 */
public class URIPathBuilder {

	private List<String> segments;

	public URIPathBuilder()
	{
		segments = new ArrayList<>();
	}

	public URIPathBuilder append(String path)
	{
		segments.add(path);
		return this;
	}

	public String build()
	{
		StringBuilder sbuilder = new StringBuilder();

		boolean first = true;
		for (String segment : segments) {
			if (first) {
				first = false;
			} else {
				sbuilder.append('/');
			}

			sbuilder.append(segment);
		}
		return sbuilder.toString();
	}
}
