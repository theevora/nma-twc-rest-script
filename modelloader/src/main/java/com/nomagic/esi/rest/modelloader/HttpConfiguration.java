/*
 * HttpConfiguration
 *
 * Revision $Revision$ $Date$
 * Author: Theewara V. (theewara_v@nomagicasia.com)
 *
 * Copyright (c) 2009 - 2017 NoMagic, Inc. All Rights Reserved.
 */
package com.nomagic.esi.rest.modelloader;

import java.net.URI;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

import javax.net.ssl.KeyManager;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.config.Registry;
import org.apache.http.config.RegistryBuilder;
import org.apache.http.conn.HttpClientConnectionManager;
import org.apache.http.conn.socket.ConnectionSocketFactory;
import org.apache.http.conn.socket.PlainConnectionSocketFactory;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.cookie.CookieSpecProvider;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Theewara V. (theewara_v@nomagicasia.com)
 */
public class HttpConfiguration {

	private static final Logger LOG = LoggerFactory.getLogger(HttpConfiguration.class);

	private final HttpClientConnectionManager connectionManager;

	private final BasicCredentialsProvider credential;

	private CloseableHttpClient httpClient;

	private final Registry<CookieSpecProvider> registry;

	private final RequestConfig requestConfig;

	private final URI uriLogout;

	public HttpConfiguration(URI uriLogout)
	{
		credential = new BasicCredentialsProvider();
		credential.setCredentials(AuthScope.ANY, new UsernamePasswordCredentials("Administrator", "Administrator"));
		registry = RegistryBuilder.<CookieSpecProvider> create().register("twc", new TwcCookieSpecProvider()).build();
		requestConfig = RequestConfig.custom().setCookieSpec("twc").build();
		connectionManager = getConnectionManager();

		this.uriLogout = uriLogout;
	}


	public HttpClientContext createContext()
	{
		HttpClientContext result;

		result = HttpClientContext.create();
		result.setCookieStore(new AgingCookieStore(Integer.getInteger(Constants.PROPERTY_COOKIE_AGING,
			Constants.COOKIE_AGING_DEFAULT).intValue()));

		return result;
	}

	public CloseableHttpClient createHttpClient()
	{
		if (httpClient == null) {
			HttpClientBuilder httpClientBuilder = HttpClients.custom() //
					.setConnectionManager(connectionManager).setDefaultRequestConfig(requestConfig)//
					.setDefaultCookieSpecRegistry(registry) //
					/*
					 * cannot enable ssl here. if the connection manager is set, SSL configs are ignored.
					 */
					//	.setSSLContext(context) //
					//	.setSSLSocketFactory(socketFactory) //
					.setDefaultCredentialsProvider(credential) //
			;

			httpClient = httpClientBuilder.build();
		}

		// httpClient can be reused - due to pooling connection manager.
		return httpClient;
	}

	public HttpClientConnectionManager getConnectionManager()
	{
		SSLContext context = null;

		try {
			SSLContext.getInstance("tls");

			TrustManager truster = new X509TrustManager() {

				@Override
				public void checkClientTrusted(X509Certificate[] arg0, String arg1) throws CertificateException
				{
					// do nothing
				}

				@Override
				public void checkServerTrusted(X509Certificate[] arg0, String arg1) throws CertificateException
				{
					// do nothing
				}

				@Override
				public X509Certificate[] getAcceptedIssuers()
				{
					return new X509Certificate[0];
				}

			};

			context = SSLContext.getInstance("tls");
			context.init(new KeyManager[0], new TrustManager[] { truster }, new SecureRandom());
		}
		catch (Exception e) {
			LOG.error(e.getMessage(), e);
			throw new RuntimeException(e);
		}

		SSLConnectionSocketFactory socketFactory = new SSLConnectionSocketFactory(context, NoopHostnameVerifier.INSTANCE);

		Registry<ConnectionSocketFactory> socketRegistry = RegistryBuilder.<ConnectionSocketFactory> create() //
				.register("http", PlainConnectionSocketFactory.getSocketFactory()) //
				.register("https", socketFactory) //
				.build();

		PoolingHttpClientConnectionManager result = new PoolingHttpClientConnectionManager(socketRegistry);
		result.setMaxTotal(20);
		result.setDefaultMaxPerRoute(20);
		return result;
	}

	/**
	 * Return a uriLogout.
	 * @return the uriLogout
	 */
	public URI getUriLogout()
	{
		return uriLogout;
	}

}
