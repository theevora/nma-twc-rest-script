/*
 * Main
 *
 * Revision $Revision$ $Date$
 * Author: Theewara V. (theewara_v@nomagicasia.com)
 *
 * Copyright (c) 2009 - 2017 NoMagic, Inc. All Rights Reserved.
 */
package com.nomagic.esi.rest.modelloader;

import java.net.URI;
import java.net.URISyntaxException;
import java.nio.charset.Charset;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

/**
 * @author Theewara V. (theewara_v@nomagicasia.com)
 */
public class Main {

	private static final Logger LOG = LoggerFactory.getLogger(Main.class);

	private static Options options;


	/**
	 * Return a options.
	 * @return the options
	 */
	public static Options getOptions()
	{
		return options;
	}


	/**
	 * Load entire model at the specific revision.
	 *
	 * Argument:
	 * argv[0] = URL to the revision to load.
	 * argv[1] = output file
	 * @param argv
	 */
	public static void main(String argv[]) throws Exception
	{
		options = processArgument(argv);
		URI uri = options.getUri();

		Path pathRevision = Paths.get(uri.getPath());

		Path pathOsmc = pathRevision.subpath(0, 1);

		String osmcPath = pathOsmc.toString();

		String pathLogout = new URIPathBuilder().append(osmcPath).append(Constants.URL_PATH_LOGOUT).build();
		URI uriLogout = new URIBuilder().setHost(uri.getHost()).setPath(pathLogout).setPort(uri.getPort()).setScheme(uri
				.getScheme()).build();

		HttpConfiguration httpConfig = new HttpConfiguration(uriLogout);

		CloseableHttpClient httpClient = httpConfig.createHttpClient();

		HttpGet getter = new HttpGet(uri);
		LOG.info("Get root objects from " + getter.getURI());

		TwcOutputWriter output = new TwcOutputWriter(options.getOutput());
		ModelRetriever.setOutput(output);

		int nthread = Integer.getInteger(Constants.PROPERTY_WORKER_NUMBER, Constants.PROPERTY_WORKER_NUMBER_DEFAULT).intValue();
		LOG.info("Prepare executor service for {} threads", Integer.toString(nthread));
		HttpWorkerThreadFactory workerFactory = new HttpWorkerThreadFactory(httpConfig);
		ModelRetriever.setExecutorService(new Scheduler(nthread, workerFactory));

		String base = null;
		try (CloseableHttpResponse response = httpClient.execute(getter)) {
			String content = IOUtils.toString(response.getEntity().getContent(), Charset.defaultCharset());
			JsonElement jcontent = Helper.parse(content);

			if (content.contains("kerml:Revision")) {
				LOG.info("The URI points to a revision.");
				LOG.debug("Revision object:\n", Helper.prettyPrint(jcontent));

				JsonObject jrevision = jcontent.getAsJsonArray().get(Constants.JSON_REVISION_INDEX).getAsJsonObject();

				JsonArray jrootObjects = jrevision.get(Constants.LDP_ATTRIBUTE_ROOT_OBJECT_IDS).getAsJsonArray();
				base = jrevision.get(Constants.LDP_ATTRIBUTE_BASE).getAsString();
				ModelRetriever.setUriDatasource(new URI(base));

				List<String> uuids = new ArrayList<>();
				jrootObjects.forEach((x) -> {
					uuids.add(x.getAsString());
				});

				ModelRetriever.getScheduler().submit(uuids, true);
			} else {
				LOG.info("The URI points to an element.");

				JsonObject jelement = jcontent.getAsJsonArray().get(Constants.JSON_ELEMENT_INDEX).getAsJsonObject();

				base = jelement.get(Constants.LDP_ATTRIBUTE_BASE).getAsString();
				ModelRetriever.setUriDatasource(new URI(base));

				String path = uri.getPath();
				int uuidStart = path.lastIndexOf('/');
				String uuid = path.substring(uuidStart + 1);

				ModelRetriever.processChildren(uuid, jcontent);
				ModelRetriever.getScheduler().submit(Collections.emptyList(), true);
			}
		}

		LOG.info("Base uri is {}", base);
	}


	/**
	 * @param argv
	 * @return
	 * @throws URISyntaxException
	 */
	private static Options processArgument(String[] argv) throws URISyntaxException
	{
		options = new Options();

		for (int i = 0; i < argv.length; ++i) {
			switch (argv[i]) {
			case "-u":
				options.setUmlOnly(true);
				break;
			case "-s":
				options.setSearchUml(true);
				break;
			default:
				options.setUri(new URI(argv[i]));

				++i;
				if (i == argv.length) {
					options.setOutput(null);
				} else {
					options.setOutput(argv[i]);
				}
				break;
			}
		}

		return options;
	}

}
