/*
 * HttpWorkerThread
 *
 * Revision $Revision$ $Date$
 * Author: Theewara V. (theewara_v@nomagicasia.com)
 *
 * Copyright (c) 2009 - 2017 NoMagic, Inc. All Rights Reserved.
 */
package com.nomagic.esi.rest.modelloader;

import org.apache.http.Header;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.impl.client.CloseableHttpClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Theewara V. (theewara_v@nomagicasia.com)
 */
public class HttpWorkerThread extends Thread {

	private static final Logger LOG = LoggerFactory.getLogger(HttpWorkerThread.class);

	private final HttpConfiguration httpConfiguration;

	private final HttpClientContext httpContext;


	/**
	 * @param httpConfiguration
	 * @param delegate
	 */
	public HttpWorkerThread(int no, HttpConfiguration httpConfiguration, Runnable delegate)
	{
		super(delegate);
		this.httpConfiguration = httpConfiguration;
		httpContext = httpConfiguration.createContext();
		setName(String.format("%s-%s", getClass().getSimpleName(), Integer.toString(no)));
	}

	/**
	 * Return a httpConfiguration.
	 * @return the httpConfiguration
	 */
	public HttpConfiguration getHttpConfiguration()
	{
		return httpConfiguration;
	}


	/**
	 * Return a httpContext.
	 * @return the httpContext
	 */
	public HttpClientContext getHttpContext()
	{
		return httpContext;
	}

	/**
	 *
	 */
	public void logout()
	{
		try {
			logoutInternal();
		}
		catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}
	}

	/**
	 *
	 */
	private void logoutInternal() throws Exception
	{
		HttpWorkerThread thread = (HttpWorkerThread)Thread.currentThread();
		CloseableHttpClient httpClient = thread.getHttpConfiguration().createHttpClient();
		AgingCookieStore acookieStore = (AgingCookieStore)thread.getHttpContext().getCookieStore();

		// time to close a session
		HttpGet getter = new HttpGet(getHttpConfiguration().getUriLogout());

		if (LOG.isDebugEnabled()) {
			LOG.debug("Logout from {} - {}", getter.getURI(), acookieStore.get("twc-rest-session-id"));
		}

		try (CloseableHttpResponse response = httpClient.execute(getter, thread.getHttpContext())) {
			for (Header header : response.getAllHeaders()) {
				LOG.debug(String.format("%s: %s\n", header.getName(), header.getValue()));
			}
		}

		acookieStore.clear();
	}


	@Override
	public void run()
	{
		try {
			super.run();
		}
		finally {
			logout();
		}
	}


}
