/*
 * TwcCookieSpecProvider
 *
 * Revision $Revision$ $Date$
 * Author: Theewara V. (theewara_v@nomagicasia.com)
 *
 * Copyright (c) 2009 - 2017 NoMagic, Inc. All Rights Reserved.
 */
package com.nomagic.esi.rest.modelloader;

import org.apache.http.impl.cookie.DefaultCookieSpecProvider;

/**
 * @author Theewara V. (theewara_v@nomagicasia.com)
 */
public class TwcCookieSpecProvider extends DefaultCookieSpecProvider {

	/**
	 *
	 */
	public TwcCookieSpecProvider()
	{
		super(CompatibilityLevel.DEFAULT, null, new String[] { "EEE, dd MMM yyyy HH:mm:ss z" }, false);
	}

}
