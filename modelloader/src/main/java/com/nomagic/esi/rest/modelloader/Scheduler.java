/*
 * Scheduler
 *
 * Revision $Revision$ $Date$
 * Author: Theewara V. (theewara_v@nomagicasia.com)
 *
 * Copyright (c) 2009 - 2017 NoMagic, Inc. All Rights Reserved.
 */
package com.nomagic.esi.rest.modelloader;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.JsonArray;

/**
 * @author Theewara V. (theewara_v@nomagicasia.com)
 */
public class Scheduler {

	private static final int BATCH_SIZE = Integer.getInteger(Constants.PROPERTY_LOADER_BATCH,
		Constants.PROPERTY_LOADER_BATCH_DEFAULT).intValue();

	private static final Logger LOG = LoggerFactory.getLogger(Scheduler.class);

	private final ThreadPoolExecutor executor;

	private final List<String> uuidQueue;

	public Scheduler(int nThreads, ThreadFactory threadFactory)
	{
		// this code is copied from java.util.concurrent.Executors.newFixedThreadPool(int, ThreadFactory);
		executor = new ThreadPoolExecutor(nThreads, nThreads, 0L, TimeUnit.MILLISECONDS, new LinkedBlockingQueue<Runnable>(),
				threadFactory);

		uuidQueue = new ArrayList<>();
	}

	public void finish()
	{
		int queued = executor.getQueue().size();
		if (LOG.isDebugEnabled()) {
			LOG.debug("Waiting {}", Integer.toString(queued));
		}
		if (queued == 0) {
			synchronized (uuidQueue) {
				if (uuidQueue.isEmpty()) {
					executor.shutdown();
				} else {
					submit(Collections.emptyList(), true);
				}
			}
		}

	}

	public void submit(List<String> uuids, boolean flush)
	{
		synchronized (uuidQueue) {
			uuidQueue.addAll(uuids);

			if (!uuidQueue.isEmpty() && uuidQueue.size() > BATCH_SIZE || flush) {
				JsonArray jids = new JsonArray();
				uuidQueue.forEach((x) -> {
					jids.add(x);
				});
				uuidQueue.clear();

				if (LOG.isDebugEnabled()) {
					LOG.debug("Submit for loading {} queue size {}", Integer.toString(jids.size()), Integer.toString(executor
							.getQueue().size()));
				}
				submit(new ModelRetriever(jids));
			}
		}
	}

	/**
	 * @param modelRetriever
	 */
	private void submit(ModelRetriever modelRetriever)
	{
		executor.submit(modelRetriever);
	}
}
