/*
 * BigJsonWriter
 *
 * Revision $Revision$ $Date$
 * Author: Theewara V. (theewara_v@nomagicasia.com)
 *
 * Copyright (c) 2009 - 2017 NoMagic, Inc. All Rights Reserved.
 */
package com.nomagic.esi.rest.modelloader;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;

/**
 * @author Theewara V. (theewara_v@nomagicasia.com)
 */
public class TwcOutputWriter {

	private static final int BATCH_SIZE = 10;

	private static final Logger LOG = LoggerFactory.getLogger(TwcOutputWriter.class);

	private long batchSize;

	private long batchStart;

	private int count;

	private final Gson gson;

	private final PrintStream printer;

	private long start;

	public TwcOutputWriter(String filename) throws FileNotFoundException
	{
		printer = new PrintStream(new FileOutputStream(filename), true);
		gson = new GsonBuilder().setPrettyPrinting().create();
		count = 0;
		start = -1;
		batchSize = 0;
	}

	/**
	 *
	 */
	public void endEntry()
	{
		printer.println(Constants.TWC_DELIMITER);
	}

	/**
	 * @param key
	 * @param size
	 */
	public void write(String key, int size)
	{
		printer.printf("TENChild %s %s\n", key, Integer.toString(size));
	}

	public synchronized void write(String key, JsonElement jelement)
	{
		if (start == -1) {
			batchStart = start = System.currentTimeMillis();
		}

		printer.printf("TEObjectK%s\n", key);
		printer.println("TEObjectV");
		printer.println(gson.toJson(jelement));

		++count;
		++batchSize;

		if (count % BATCH_SIZE == 0) {
			long current = System.currentTimeMillis();
			double speedOverall = (count * 1000.0) / (current - start);
			double speedCurrent = (batchSize * 1000.0) / (current - batchStart);
			batchSize = 0;
			batchStart = current;
			LOG.info(String.format("Retrieved: %s elements. Retrieving rate: %s elements/sec, %s elements/sec", Integer.toString(
				count), Double.toString(speedOverall), Double.toString(speedCurrent)));
		}
	}
}
