/*
 * HttpThreadFactory
 *
 * Revision $Revision$ $Date$
 * Author: Theewara V. (theewara_v@nomagicasia.com)
 *
 * Copyright (c) 2009 - 2017 NoMagic, Inc. All Rights Reserved.
 */
package com.nomagic.esi.rest.modelloader;

import java.util.concurrent.ThreadFactory;

/**
 * @author Theewara V. (theewara_v@nomagicasia.com)
 */
public class HttpWorkerThreadFactory implements ThreadFactory {

	private int count;

	private final HttpConfiguration httpConfiguration;

	public HttpWorkerThreadFactory(HttpConfiguration httpConfiguration)
	{
		this.httpConfiguration = httpConfiguration;
		count = 0;
	}

	@Override
	public Thread newThread(Runnable delegate)
	{
		return new HttpWorkerThread(++count, httpConfiguration, delegate);
	}
}
