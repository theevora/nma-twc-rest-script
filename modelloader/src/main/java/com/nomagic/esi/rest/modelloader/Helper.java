/*
 * Helper
 *
 * Revision $Revision$ $Date$
 * Author: Theewara V. (theewara_v@nomagicasia.com)
 *
 * Copyright (c) 2009 - 2017 NoMagic, Inc. All Rights Reserved.
 */
package com.nomagic.esi.rest.modelloader;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

/**
 * @author Theewara V. (theewara_v@nomagicasia.com)
 */
public class Helper {

	public static JsonElement parse(String content)
	{
		JsonParser parser = new JsonParser();
		JsonElement jcontent = parser.parse(content);
		return jcontent;
	}

	public static String prettyPrint(JsonElement jelement)
	{
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		return gson.toJson(jelement);
	}
}
