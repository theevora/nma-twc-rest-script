/*
 * Constants
 *
 * Revision $Revision$ $Date$
 * Author: Theewara V. (theewara_v@nomagicasia.com)
 *
 * Copyright (c) 2009 - 2017 NoMagic, Inc. All Rights Reserved.
 */
package com.nomagic.esi.rest.modelloader;

/**
 * @author Theewara V. (theewara_v@nomagicasia.com)
 */
public class Constants {

	public static final Integer COOKIE_AGING_DEFAULT = Integer.valueOf(1000000);

	public static final String COOKIE_SESSION_ID = "twc-rest-session-id";

	public static final int JSON_ELEMENT_INDEX = 1;

	public static final int JSON_REVISION_INDEX = 0;

	public static final String LDP_ATTRIBUTE_BASE = "@base";

	public static final String LDP_ATTRIBUTE_ROOT_OBJECT_IDS = "rootObjectIDs";

	public static final String LDP_CONTAINS = "ldp:contains";

	public static final String LDP_ID = "@id";

	public static final String PROPERTY_COOKIE_AGING = "loader.cookie.aging";

	public static final String PROPERTY_LOADER_BATCH = "loader.batch";

	public static final int PROPERTY_LOADER_BATCH_DEFAULT = 32;

	public static final String PROPERTY_WORKER_NUMBER = "loader.worker.number";

	public static final int PROPERTY_WORKER_NUMBER_DEFAULT = 1;

	public static final String TWC_DELIMITER = "\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"";

	public static final String URL_PATH_LOGOUT = "logout";
}
