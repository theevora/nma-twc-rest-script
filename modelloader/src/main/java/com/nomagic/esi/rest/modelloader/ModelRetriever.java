/*
 * ModelRetriever
 *
 * Revision $Revision$ $Date$
 * Author: Theewara V. (theewara_v@nomagicasia.com)
 *
 * Copyright (c) 2009 - 2017 NoMagic, Inc. All Rights Reserved.
 */
package com.nomagic.esi.rest.modelloader;

import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map.Entry;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;


/**
 * @author Theewara V. (theewara_v@nomagicasia.com)
 */
public class ModelRetriever implements Runnable {

	private static final Logger LOG = LoggerFactory.getLogger(ModelRetriever.class);

	private static TwcOutputWriter output;

	private static Scheduler scheduler;

	private static URI uriDatasource;

	/**
	 * Return a executorService.
	 * @return the executorService
	 */
	public static Scheduler getScheduler()
	{
		return scheduler;
	}


	public static void processChildren(String key, JsonElement jelement)
	{
		try {

			output.write(key, jelement);

			JsonArray jldpContent;
			if (jelement.isJsonArray()) {
				jldpContent = jelement.getAsJsonArray();
			} else if (jelement.getAsJsonObject().get("status").getAsInt() == 200) {
				jldpContent = jelement.getAsJsonObject().get("data").getAsJsonArray();
			} else {
				return;
			}

			JsonObject containmentTriple = jldpContent.get(0).getAsJsonObject();


			if (Main.getOptions().isSearchUml() || Main.getOptions().isUmlOnly()) {
				String type = jldpContent.get(Constants.JSON_ELEMENT_INDEX).getAsJsonObject().get("@type").getAsString();

				boolean uml = type.startsWith("uml:");

				if (Main.getOptions().isSearchUml() && uml) {
					LOG.info("Found uml element at {}/{}", uriDatasource, key);
					System.exit(0);
				}

				if (Main.getOptions().isUmlOnly() && !uml) {
					return;
				}
			}

			JsonArray jcontains = containmentTriple.get(Constants.LDP_CONTAINS).getAsJsonArray();

			// sort by uuid
			List<String> childIds = new ArrayList<>();
			for (JsonElement jchildren : jcontains) {
				String id = jchildren.getAsJsonObject().get(Constants.LDP_ID).getAsString();
				childIds.add(id);
			}

			Collections.sort(childIds);

			output.write(key, childIds.size());

			ModelRetriever.getScheduler().submit(childIds, false);
		}
		catch (IllegalStateException ise) {
			LOG.error(ise.getMessage(), ise);
		}
		finally {
			output.endEntry();
		}
	}


	/**
	 * Set a executorService.
	 * @param executorService the executorService to set
	 */
	public static void setExecutorService(Scheduler scheduler)
	{
		ModelRetriever.scheduler = scheduler;
	}

	/**
	 * Set a output.
	 * @param output the output to set
	 */
	public static void setOutput(TwcOutputWriter output)
	{
		ModelRetriever.output = output;
	}


	/**
	 * Set a uriDatasource.
	 * @param uriDatasource the uriDatasource to set
	 */
	public static void setUriDatasource(URI uriDatasource)
	{
		ModelRetriever.uriDatasource = uriDatasource;
	}

	private final JsonArray jids;

	public ModelRetriever(JsonArray jids)
	{
		this.jids = jids;
	}

	@Override
	public void run()
	{
		try {
			runInternal();
		}
		catch (IOException e) {
			LOG.error(e.getMessage(), e);
		}

		getScheduler().finish();
	}

	private void runInternal() throws IOException
	{
		if (jids.size() == 0) {
			return;
		}

		HttpWorkerThread thread = (HttpWorkerThread)Thread.currentThread();
		CloseableHttpClient httpClient = thread.getHttpConfiguration().createHttpClient();
		AgingCookieStore acookieStore = (AgingCookieStore)thread.getHttpContext().getCookieStore();

		LOG.info("Download {} elements", Integer.toString(jids.size()));

		if (!acookieStore.isAlive()) {
			thread.logout();
		}

		HttpPost request = new HttpPost(uriDatasource);

		StringBuilder sbuilder = new StringBuilder();
		boolean first = true;
		for (JsonElement je : jids) {
			if (first) {
				first = false;
			} else {
				sbuilder.append(',');
			}
			sbuilder.append(je.getAsString());
		}

		HttpEntity entity = new StringEntity(sbuilder.toString());
		request.setEntity(entity);

		try (CloseableHttpResponse response = httpClient.execute(request, thread.getHttpContext())) {
			if (LOG.isDebugEnabled()) {
				LOG.debug("query {}", acookieStore.get("twc-rest-session-id"));
			}

			String content = IOUtils.toString(response.getEntity().getContent(), "utf8");

			JsonObject jcontent = Helper.parse(content).getAsJsonObject();

			for (Entry<String, JsonElement> answer : jcontent.entrySet()) {
				processChildren(answer.getKey(), answer.getValue());
			}
		}
		catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}

	}
}
