/*
 * Options
 *
 * Revision $Revision$ $Date$
 * Author: Theewara V. (theewara_v@nomagicasia.com)
 *
 * Copyright (c) 2009 - 2017 NoMagic, Inc. All Rights Reserved.
 */
package com.nomagic.esi.rest.modelloader;

import java.net.URI;

/**
 * @author Theewara V. (theewara_v@nomagicasia.com)
 */
public class Options {

	private String output;

	private boolean searchUml;

	private boolean umlOnly;

	private URI uri;


	/**
	 * Return a output.
	 * @return the output
	 */
	public String getOutput()
	{
		return output;
	}

	/**
	 * Return a uri.
	 * @return the uri
	 */
	public URI getUri()
	{
		return uri;
	}

	/**
	 * Return a searchUml.
	 * @return the searchUml
	 */
	public boolean isSearchUml()
	{
		return searchUml;
	}


	/**
	 * Return a umlOnly.
	 * @return the umlOnly
	 */
	public boolean isUmlOnly()
	{
		return umlOnly;
	}

	/**
	 * @param string
	 */
	public void setOutput(String output)
	{
		this.output = output;
	}


	/**
	 * Set a searchUml.
	 * @param searchUml the searchUml to set
	 */
	public void setSearchUml(boolean searchUml)
	{
		this.searchUml = searchUml;
	}


	/**
	 * Set a umlOnly.
	 * @param umlOnly the umlOnly to set
	 */
	public void setUmlOnly(boolean umlOnly)
	{
		this.umlOnly = umlOnly;
	}

	/**
	 * Set a uri.
	 * @param uri the uri to set
	 */
	public void setUri(URI uri)
	{
		this.uri = uri;
	}


}
