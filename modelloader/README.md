# Model Loader
Model Loader loads entire resource or an element with its all children hierarchy.

## Build
To build the tool. 
> ./gradlew build

## Run
The tool accepts two arguments: the starting URL and the output file. The starting URL can be

* A revision URL, for example, .../revisions/5. The revision URL returns first-level objects in a resource. All objects are downloaded.
* A URL to an element, for example, .../elements/{element UUID}. The given element and its all children hierarchy are downloaded.

## How to find Element ID in MagicDraw
The element UUID refers to _Element Server ID_ in MagicDraw. To find an element server ID:
1. Open the **Specification** dialog for a particular element
1. Click the root element in the tree in the left pane.
1. Find the attribute **Element Server ID** in the table in the right pane. If **Element Server ID** is not listed, select **All** in the combo box on the top right of the table.

